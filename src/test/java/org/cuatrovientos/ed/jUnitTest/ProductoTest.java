package org.cuatrovientos.ed.jUnitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.cuatrovientos.ed.jUnit.Producto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProductoTest {
	
	Producto p1;
	
	@BeforeEach
	void setUp() throws Exception {
		p1 = new Producto("pan", 0.50f, 1);
	}

	@Test
	void testGetName() {
		String expected = "pan";
		String actual = p1.getNombre();
		assertEquals(expected, actual);
	}

	@Test
	void testPrecioTotal() {
		 float expected = 0.50f;
		 float actual = p1.precioTotal();
		 assertEquals(expected, actual);
	}
}
