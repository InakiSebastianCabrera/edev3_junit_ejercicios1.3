package org.cuatrovientos.ed.jUnitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.cuatrovientos.ed.jUnit.Factura;
import org.cuatrovientos.ed.jUnit.Producto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FacturaTest {
	
	Factura f1;
	
	@BeforeEach
	void setUp() throws Exception {
		f1 = new Factura();
		f1.meterProducto(new Producto("pan" , 10,4));
	}

	@Test
	void testTotalFactura() {
		float expected = 40f;
		float actual = f1.totalFactura();
		assertEquals(expected, actual);
	}
	
	@Test
	void testAplicarIva() {

		float expected = 48.4f;
		float actual = f1.aplicarIva(0.21f);
		assertEquals(expected, actual);
	}

}
