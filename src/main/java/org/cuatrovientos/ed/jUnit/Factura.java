package org.cuatrovientos.ed.jUnit;

import java.util.ArrayList;

public class Factura {
	ArrayList<Producto> productos = new ArrayList<Producto>();
	
	public void meterProducto(Producto p) {
		productos.add(p);
	}
	
	public float totalFactura() {
		float total = 0;
		for (Producto p1 : productos) {
			total += p1.precioTotal(); 
		}
		return total;
	}
	
	public float aplicarIva(Float iva) {
		float precioTotal = totalFactura();
		return (precioTotal * iva) + precioTotal;
	}
}
