package org.cuatrovientos.ed.jUnit;

public class Producto {
	String nombre;
	float precio;
	int cantidad;
	public Producto(String nombre, float precio, int cantidad) {
		super();
		this.nombre = nombre;
		this.precio = precio;
		this.cantidad = cantidad;
	}
	
	public float precioTotal() {
		
		return precio * cantidad;
		
	}
	
	public String toString() {
		
		return nombre + ": " + precio + "� Cantidad: " + cantidad; 
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
